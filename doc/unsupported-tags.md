## Unsupported tags

**CUDA Container Support Policy**

CUDA image container tags have a lifetime. The tags will be deleted Six Months after the last supported "Tesla Recommended Driver" has gone end-of-life OR a newer update release has been made for the same CUDA version.

Please see [CUDA Container Support Policy](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/doc/support-policy.md) for more information.


> :warning: These tags still exist and may contain critical vulnerabilities.<br> 
>   _Use at your own risk._


### ubuntu22.04
#### CUDA 12.1.0
- `12.1.0-base-ubuntu22.04`
- `12.1.0-cudnn8-devel-ubuntu22.04`
- `12.1.0-cudnn8-runtime-ubuntu22.04`
- `12.1.0-devel-ubuntu22.04`
- `12.1.0-runtime-ubuntu22.04`
#### CUDA 12.0.0
- `12.0.0-base-ubuntu22.04`
- `12.0.0-cudnn8-devel-ubuntu22.04`
- `12.0.0-cudnn8-runtime-ubuntu22.04`
- `12.0.0-devel-ubuntu22.04`
- `12.0.0-runtime-ubuntu22.04`
### ubuntu20.04
#### CUDA 12.1.0
- `12.1.0-base-ubuntu20.04`
- `12.1.0-cudnn8-devel-ubuntu20.04`
- `12.1.0-cudnn8-runtime-ubuntu20.04`
- `12.1.0-devel-ubuntu20.04`
- `12.1.0-runtime-ubuntu20.04`
#### CUDA 12.0.0
- `12.0.0-base-ubuntu20.04`
- `12.0.0-cudnn8-devel-ubuntu20.04`
- `12.0.0-cudnn8-runtime-ubuntu20.04`
- `12.0.0-devel-ubuntu20.04`
- `12.0.0-runtime-ubuntu20.04`
### ubuntu18.04
#### CUDA 12.1.0
- `12.1.0-base-ubuntu18.04`
- `12.1.0-devel-ubuntu18.04`
- `12.1.0-runtime-ubuntu18.04`
#### CUDA 12.0.0
- `12.0.0-base-ubuntu18.04`
- `12.0.0-cudnn8-devel-ubuntu18.04`
- `12.0.0-cudnn8-runtime-ubuntu18.04`
- `12.0.0-devel-ubuntu18.04`
- `12.0.0-runtime-ubuntu18.04`
### ubi9
#### CUDA 12.1.0
- `12.1.0-base-ubi9`
- `12.1.0-cudnn8-devel-ubi9`
- `12.1.0-cudnn8-runtime-ubi9`
- `12.1.0-devel-ubi9`
- `12.1.0-runtime-ubi9`
### ubi8
#### CUDA 12.1.0
- `12.1.0-base-ubi8`
- `12.1.0-cudnn8-devel-ubi8`
- `12.1.0-cudnn8-runtime-ubi8`
- `12.1.0-devel-ubi8`
- `12.1.0-runtime-ubi8`
#### CUDA 12.0.0
- `12.0.0-base-ubi8`
- `12.0.0-cudnn8-devel-ubi8`
- `12.0.0-cudnn8-runtime-ubi8`
- `12.0.0-devel-ubi8`
- `12.0.0-runtime-ubi8`
### ubi7
#### CUDA 12.1.0
- `12.1.0-base-ubi7`
- `12.1.0-cudnn8-devel-ubi7`
- `12.1.0-cudnn8-runtime-ubi7`
- `12.1.0-devel-ubi7`
- `12.1.0-runtime-ubi7`
#### CUDA 12.0.0
- `12.0.0-base-ubi7`
- `12.0.0-cudnn8-devel-ubi7`
- `12.0.0-cudnn8-runtime-ubi7`
- `12.0.0-devel-ubi7`
- `12.0.0-runtime-ubi7`
### rockylinux9
#### CUDA 12.1.0
- `12.1.0-base-rockylinux9`
- `12.1.0-cudnn8-devel-rockylinux9`
- `12.1.0-cudnn8-runtime-rockylinux9`
- `12.1.0-devel-rockylinux9`
- `12.1.0-runtime-rockylinux9`
### rockylinux8
#### CUDA 12.1.0
- `12.1.0-base-rockylinux8`
- `12.1.0-devel-rockylinux8`
- `12.1.0-runtime-rockylinux8`
#### CUDA 12.0.0
- `12.0.0-base-rockylinux8`
- `12.0.0-cudnn8-devel-rockylinux8`
- `12.0.0-cudnn8-runtime-rockylinux8`
- `12.0.0-devel-rockylinux8`
- `12.0.0-runtime-rockylinux8`
### centos7
#### CUDA 12.1.0
- `12.1.0-base-centos7`
- `12.1.0-cudnn8-devel-centos7`
- `12.1.0-cudnn8-runtime-centos7`
- `12.1.0-devel-centos7`
- `12.1.0-runtime-centos7`
#### CUDA 12.0.0
- `12.0.0-base-centos7`
- `12.0.0-cudnn8-devel-centos7`
- `12.0.0-cudnn8-runtime-centos7`
- `12.0.0-devel-centos7`
- `12.0.0-runtime-centos7`
